#Q1.Write a python program to swap two numbers using a third variable.x = 10 y=5
# inputs from the user
x = input('Enter value of x: ')
y = input('Enter value of y: ')

# create a temporary variable and swap the values
temp = x
x = y
y = temp

print('The value of x after swapping:',x)
print('The value of y after swapping: ',y)
